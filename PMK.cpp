#include <stdlib.h>
#include <time.h>
#include <cassert>
#include <iostream>
#include <fstream>
#include "point_set/mutable-point-set-list.h"
#include "point_set/point-set-list.h"
#include "point_set/point-ref.h"
#include "point_set/point-set.h"
#include "clustering/hierarchical-clusterer.h"
#include "util/distance-computer.h"
#include "pyramids/pyramid-matcher.h"
#include "pyramids/pyramid-maker.h"
#include "pyramids/input-specific-vg-pyramid-maker.h"
#include "pyramids/uniform-pyramid-maker.h"
#include "histograms/multi-resolution-histogram.h"
#include "experiment/svm-experiment.h"
//#include "eth-selector.h"
#include <Python.h>
#include <numpy/arrayobject.h>

using namespace std;
using namespace libpmk;
using namespace libpmk_util;

PyObject* _compute_pmk(const PointSetList& psl, PyramidMaker* pm, int from);
PyObject* _compute_pmk1(const PointSetList& psl1, const PointSetList& psl2, 
PyramidMaker* pm1, PyramidMaker* pm2);

PointSetList* prepare_data(PyListObject *data){
    MutablePointSetList* psl = new MutablePointSetList();
    int nSets = PyList_Size((PyObject*)data); //PyArray_DIM(data, 0);
    for(int k = 0; k < nSets; ++k){
        PyArrayObject* arr = (PyArrayObject*) PyList_GetItem((PyObject*) data, k);
        int nPoints = PyArray_DIM(arr, 0);
        int nDims = PyArray_DIM(arr, 1);
        PointSet ps = PointSet(nDims);
        for(int i = 0; i < nPoints; ++i){
            Point p = Point(nDims);
            for(int j = 0; j < nDims; ++j){
                void *ptr = PyArray_GETPTR2(arr, i, j);
                double v = PyFloat_AS_DOUBLE( PyArray_GETITEM(arr, ptr) );
                p.set_feature(0, v);
            }
            p.set_weight(1);
            ps.add_point(p);
        }
        psl->add_point_set(ps);
    }

    return psl;
}

static PyObject* compute_pmk_uniform(PyObject* self, PyObject* args){
    double finest_side_length = 0.01;
    double side_length_factor = 2;
    int discretize_order = 2;
    int from = 0;
    bool do_translations = true;
    bool global_translations = true;
    PyListObject *data;

    if (!PyArg_ParseTuple(args, "O!ddiibb", &PyList_Type, &data, &finest_side_length, 
    &side_length_factor, &discretize_order, &from, &do_translations, &global_translations))
        return NULL;

    PointSetList* psl = prepare_data(data);
    UniformPyramidMaker upm;
    upm.Preprocess(*psl, finest_side_length, side_length_factor, discretize_order, do_translations, global_translations);  
    PyObject* result = _compute_pmk(*psl, &upm, from);

    Py_INCREF(result);
    delete psl;

    return result;
}

static PyObject* compute_pmk_uniform1(PyObject* self, PyObject* args){
    double finest_side_length = 0.01;
    double side_length_factor = 2;
    int discretize_order = 2;
    bool do_translations = true;
    bool global_translations = true;
    PyListObject *X;
    PyListObject *Y;

    if (!PyArg_ParseTuple(args, "O!O!ddibb", &PyList_Type, &X, &PyList_Type, &Y, &finest_side_length, 
    &side_length_factor, &discretize_order, &do_translations, &global_translations))
        return NULL;

    PointSetList* pslX = prepare_data(X);
    PointSetList* pslY = prepare_data(Y);
    UniformPyramidMaker upmX;
    UniformPyramidMaker upmY;
    upmX.Preprocess(*pslX, finest_side_length, side_length_factor, discretize_order, do_translations, global_translations);  
    upmY.Preprocess(*pslY, finest_side_length, side_length_factor, discretize_order, do_translations, global_translations);  
    PyObject* result = _compute_pmk1(*pslX, *pslY, &upmX, &upmY);

    Py_INCREF(result);
    delete pslX;
    delete pslY;

    return result;
}

static PyObject* compute_pmk_input_specific_vg(PyObject* self, PyObject* args)
{
    PyListObject *data;
    int num_levels;
    int branch_factor;
    int from;
    
    if (!PyArg_ParseTuple(args, "O!iii", &PyList_Type, &data, &num_levels, &branch_factor, &from))
        return NULL;

    PointSetList* psl = prepare_data(data);
    vector<PointRef> point_refs;
    psl->GetPointRefs(&point_refs);

    HierarchicalClusterer* clusterer = new HierarchicalClusterer();
    L2DistanceComputer dist_computer;
    clusterer->Cluster(num_levels, branch_factor, point_refs, dist_computer);
    InputSpecificVGPyramidMaker* vgpm = new InputSpecificVGPyramidMaker(*clusterer, dist_computer);

    //cout << "Making pyramids" << endl;
    PyObject* result = _compute_pmk(*psl, vgpm, from);

    delete vgpm;
    delete clusterer;
    delete psl;

    Py_INCREF(result);
    return result;
}

PyObject* _compute_pmk1(const PointSetList& psl1, const PointSetList& psl2, 
PyramidMaker* pm1, PyramidMaker* pm2)
{
    //cout << "Running hierarchical clustering" << endl;
    vector<MultiResolutionHistogram*> vec1 = pm1->MakePyramids(psl1);
    vector<MultiResolutionHistogram*> vec2 = pm2->MakePyramids(psl2);
    long int dims[] = {vec1.size(), vec2.size()};
    PyArrayObject *out = (PyArrayObject*) PyArray_ZEROS(2, dims, NPY_DOUBLE, 0);
    double tmp;
    //cout << "Computing kernel matrix" << endl;
    #pragma omp parallel for
    for (int ii = 0; ii < (int) vec1.size(); ++ii) {
        #pragma omp parallel for
        for (int jj = 0; jj < (int) vec2.size(); ++jj) {
            void *ptr = PyArray_GETPTR2(out, ii, jj);
            tmp = PyramidMatcher::GetPyramidMatchSimilarity(
            *vec1[ii], *vec2[jj], BIN_WEIGHT_GLOBAL); //BIN_WEIGHT_GLOBAL); // BIN_WEIGHT_INPUT_SPECIFIC
            PyArray_SETITEM(out, ptr, PyFloat_FromDouble(tmp));
        }
    }

    // Free some memory:
    for (int ii = 0; ii < (int)vec1.size(); ++ii) {
        delete vec1[ii];
    }
    // Free some memory:
    for (int ii = 0; ii < (int)vec2.size(); ++ii) {
        delete vec2[ii];
    }

    //Py_INCREF(out);
    return (PyObject*) out;
}

PyObject* _compute_pmk(const PointSetList& psl, PyramidMaker* pm, int from)
{
    //cout << "Running hierarchical clustering" << endl;
    vector<MultiResolutionHistogram*> vec = pm->MakePyramids(psl);
    KernelMatrix km(vec.size());

    //cout << "Computing kernel matrix" << endl;
    #pragma omp parallel for
    for (int ii = from; ii < (int)vec.size(); ++ii) {
        #pragma omp parallel for
        for (int jj = 0; jj <= ii; ++jj) {
            km.at(ii, jj) = PyramidMatcher::GetPyramidMatchSimilarity(
            *vec[ii], *vec[jj], BIN_WEIGHT_GLOBAL); //BIN_WEIGHT_GLOBAL); // BIN_WEIGHT_INPUT_SPECIFIC
        }
    }

    //km.Normalize();

    int r, c;
    long int dims[] = {vec.size(), vec.size()};
    PyArrayObject *out = (PyArrayObject*) PyArray_ZEROS(2, dims, NPY_DOUBLE, 0);
    
    for(r = from; r < vec.size(); r++) {
        for(c = 0; c < vec.size(); c++) {
            void *ptr = PyArray_GETPTR2(out, r, c);
            PyArray_SETITEM(out, ptr, PyFloat_FromDouble(km.at(r,c)));
        }
    }

    // Free some memory:
    for (int ii = 0; ii < (int)vec.size(); ++ii) {
        delete vec[ii];
    }
    //Py_INCREF(out);
    return (PyObject*) out;
}


/*  define functions in module */
static PyMethodDef Methods[] =
{
     {"compute_pmk_uniform", compute_pmk_uniform, METH_VARARGS,
         "compute the pyramid match kernel using uniform bins"},

     {"compute_pmk_uniform1", compute_pmk_uniform1, METH_VARARGS,
         "compute the pyramid match kernel using uniform bins (X, Y) version"},

     {"compute_pmk_input_specific_vg", compute_pmk_input_specific_vg, METH_VARARGS,
         "compute the pyramid match kernel using input specific vocabulari guided bins"},
     {NULL, NULL, 0, NULL}
};

/* module initialization */
PyMODINIT_FUNC
initpmk(void)
{
     (void) Py_InitModule("pmk", Methods);
     /* IMPORTANT: this must be called */
     import_array();
}
