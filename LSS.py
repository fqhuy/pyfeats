# -*- coding: utf-8 -*-
"""
Created on Sun Oct 20 09:38:51 2013

@author: phan
"""

from Feature import for_all_patches, normalize, FeatureExtractor
from lss import *


def extract_feature_lss(image, patch_size=5, cor_size = 40, \
    nrad = 3, nang = 12, var_noise = 300000, saliency_thresh = 0.7, \
    homogeneity_thresh = 0.7, snn_thresh = 0.85,calc_rect=(0,0,0,0),pruned=False):
        
    parms = {'patch_size' : patch_size,'cor_size' : cor_size, \
    'nrad' : nrad, 'nang' : nang, 'var_noise' : var_noise, \
    'saliency_thresh' : saliency_thresh, \
    'homogeneity_thresh' :  homogeneity_thresh, \
    'snn_thresh' : snn_thresh}
    #calc_rect = calc_rect
    
    features = calc_ssdescs_alt(image=np.asfortranarray(image,dtype='f8') , \
    parms=parms, calc_rect=calc_rect,pruned=pruned)
    
    return features
    
class LSS(FeatureExtractor):
    def __init__(self,patch_size=(5,5), cor_size = 40, \
    nrad = 3, nang = 12, var_noise = 300000, saliency_thresh = 0.7, \
    homogeneity_thresh = 0.7, snn_thresh = 0.7,calc_rect=(0,0,0,0)):
        super(LSS, self).__init__(patch_size,0,'none')
        self.cor_size = cor_size
        self.nrad = nrad
        self.nang = nang
        self.var_noise = var_noise
        self.saliency_thresh = saliency_thresh
        self.homogeneity_thresh = homogeneity_thresh
        self.snn_thresh = snn_thresh
        self.calc_rect = calc_rect
        
    def extract(self,image):
        #extract_feature
        return extract_feature_lss(image, self.patch_size, self.cor_size, \
        self.nrad, self.nang, self.var_noise, self.saliency_thresh, \
        self.homogeneity_thresh, self.snn_thresh,self.calc_rect)
        
    def size(self):
        return 0
        
if __name__ == "__main__":
    from skimage.io import imread
    from skimage.color import rgb2gray
    
    im = imread('/Users/huphan-osx/perforce/patternista/data/CERAMICS/img/img_8.jpg')
    #feat = extract_feature_lss(im,calc_rect=(350,350,450,450),pruned=True)
    feat = extract_feature_lss(im[300:500, 300:500,:],calc_rect=(42,42,200-42-1,200-42-1),pruned=True)
    np.savetxt("lss_features.txt",feat)
    