# -*- coding: utf-8 -*-
"""
Created on Sun Oct 20 09:24:35 2013

@author: phan
"""
from Feature import for_all_patches, normalize, FeatureExtractor
import numpy as np
from scipy.fftpack import dct

def extract_feature_dct(image, patch_size=(8,8), spacing=8, normalizer='weber', padding='None'):
    def _extract_feature_dct(patch):
        return normalize(dct(patch).flatten(),normalizer)
    return np.asfarray(for_all_patches(image, _extract_feature_dct, patch_size, spacing))
    
class DCT(FeatureExtractor):
    def __init__(self,patch_size,spacing,normalizer='weber',padding='none'):
        super(DCT, self).__init__(patch_size,spacing,normalizer,padding)
        
    def extract(self,image):
        #extract_feature
        return extract_feature_dct(image, self.patch_size, \
        self.spacing,self.normalizer,self.padding)
        
    def size(self):
        return self.patch_size[0] * self.patch_size[1]
