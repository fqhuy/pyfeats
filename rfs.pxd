# -*- coding: utf-8 -*-
"""
Created on Fri Mar 14 19:09:22 2014

@author: phan
"""
cimport numpy as np
import numpy as np

cdef extern from "anigauss.h":
    void anigauss(float *input, float *output, int sizex, int sizey, 
                  double sigmav, double sigmau, double phi, int orderv, int orderu)
                  
