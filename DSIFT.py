# -*- coding: utf-8 -*-
"""
Created on Sun Oct 20 09:51:37 2013

@author: phan
"""
import numpy as np
import vlfeat as vl
import math
import logging
from Feature import FeatureExtractor
#from Feature import for_all_paches, normalize, FeatureExtractor
    
def extract_feature_dsift(image,cell_size=(2,2),spacing=8,padding=1,n_bins=(4,4),bounds=None,pca=None):
    shape = image.shape
    if bounds is not None:
        image = image[bounds[1]:bounds[3], bounds[0] : bounds[2]]
    sift = vl.DSIFT(-1,-1,spacing, cell_size[0]) #image.shape[0], image.shape[1],
    c_array  = np.ascontiguousarray(image,dtype='f')
    data = sift.process(c_array)
    dx = math.ceil((image.shape[1] - (cell_size[1] * (n_bins[1] - 1) + 1) + 1) / float(spacing))
    dy = math.ceil((image.shape[0] - (cell_size[0] * (n_bins[0] - 1) + 1)  + 1) / float(spacing))
    data = data.reshape((dy,dx,data.shape[1]))
    if padding != 0:
        ey = round(shape[0] / float(spacing ))
        pady1 = round((ey - data.shape[0]) / 2)
        pady2 = ey - data.shape[0] - pady1
        ex = round(shape[1] / float(spacing))
        padx1 = round((ex - data.shape[1]) / 2)
        padx2 = ex - data.shape[1] - padx1
        data = np.pad(data, ((pady1 + padding, pady2 + padding),(padx1 + padding, padx2 + padding),(0,0)), 'constant')
    return data

class DSIFT(FeatureExtractor):
    def __init__(self,cell_size=(2,2),spacing=8,n_bins=(4,4),bounds=None,normalizer='weber',padding=1,pca=None):
        super(DSIFT, self).__init__(cell_size[0] * n_bins[0],spacing,normalizer,padding)
        self.bounds = bounds
        self.n_bins = n_bins
        self.cell_size = cell_size
        self.pca = pca
        
    def extract(self,image):
        #extract_feature
        return extract_feature_dsift(image, self.cell_size, \
        self.spacing, self.n_bins, self.normalizer,self.padding,self.pca)
        
    def feature_vector_shape(self,data):
        dx = math.ceil((data.shape[1] - (self.cell_size[1] * (self.n_bins[1] - 1) + 1) + 1) / float(self.spacing)) + self.padding * 2
        dy = math.ceil((data.shape[0] - (self.cell_size[0] * (self.n_bins[0] - 1) + 1) + 1) / float(self.spacing)) + self.padding * 2       
        return (dy,dx)
            
    def size(self):
        return 128

if __name__ == "__main_":    
    from PIL import Image
    from skimage.transform import pyramid_reduce
    from sklearn.decomposition import PCA
    from skimage.io import imread
    import os
    
    root = '/media/phan/BIGDATA/CLIMAZ/NEG/'
    saveroot = root[:-1] + '_OUTPUT/'
    
    pca = PCA(n_components=64)
    pca_data = np.load(saveroot + 'pca.npy')
    pca.components_ = pca_data[0]
    pca.explained_variance_ratio_ = pca_data[1]
    pca.mean_ = pca_data[2]
    
    files = os.listdir(root)
    files = [f for f in files if f.endswith('.jpg')]
    interval = 5
    maxScale = 5
    for f in files[0:20]:
        #image = np.array(Image.open(root + f).convert('F'))
        image = imread(root + f,as_grey=True)
        scaled = image
        for i in range(interval):
            scale = pow(2.0, i / float(interval))

            if scale > 1:
                scaled = pyramid_reduce(image, scale)
            sift  = extract_feature_dsift(scaled,spacing = 4)
            sift = pca.transform(sift)
            np.save(saveroot + f[:-4] + '_l' + str(i) + '_sift.npy', np.asarray(sift,dtype='f'))
            
            if i + interval <= maxScale:
                sift = extract_feature_dsift(scaled,spacing = 8)
                sift = pca.transform(sift)
                np.save(saveroot + f[:-4] + '_l' + str(i + interval) + '_sift.npy', np.asarray(sift,dtype='f'))
            j = 2
            while i + j * interval <= maxScale:
                scaled = pyramid_reduce(scaled, 0.5)
                sift = (extract_feature_dsift(scaled,spacing = 8))
                sift = pca.transform(sift)
                np.save(saveroot + f[:-4] + '_l' + str(i + j * interval) + '_sift.npy', np.asarray(sift, dtype='f'))
                j = j + 1        

if __name__ == "__main__":
    import sys
    import os
    from skimage.transform import rescale, pyramid_reduce #pyramid_gaussian, pyramid_expand, pyramid_reduce
    from skimage import img_as_ubyte
    from skimage.io import imread
    import logging as lgg
    import pyramid as pyr
    from sklearn.decomposition import PCA
 
    root = '/media/phan/BIGDATA/CLIMAZ/SCM_CLIM2b/'
    saveroot = root[:-1] + '_OUTPUT/'
    
    lgg.basicConfig(filename=saveroot + "features2.log", level=lgg.DEBUG, \
    format='%(asctime)s:%(levelname)s:%(message)s')
    lgg.info("-----------------------Started---------------------------------")      
    maxScale = 5
    if len(sys.argv) >= 2:
        root = sys.argv[1]
    
    if len(sys.argv) == 3:
        maxScale = int(sys.argv[2])
        
    #pca = PCA(n_components=64)
    #pca_data = np.load(saveroot + 'pca.npy')
    #pca.components_ = pca_data[0]
    #pca.explained_variance_ratio_ = pca_data[1]
    #pca.mean_ = pca_data[2]
    
    #classes = ['fleece', 'fur','knit1']
    #classes = ['corduroy', 'denim']
    #classes = ['knit1']
    classes = ['fleece', 'fur', 'corduroy', 'denim','knit1', 'leather', 'boucle', 'lace']
    #os.listdir(root)
    for d in classes:
        lgg.info('---------------- CLASS %s ------------------\n' % d)
        if os.path.isdir(root + d):
            for i in range(0,60): #60 Images
                lgg.info('processing image: %s\n' % str(i))
                fname = root + d + '/image_' + str(i)
                skip = True
                for j in range(6):
                    if not os.path.isfile(saveroot + d + '/image_' + str(i) + '_l' + str(j) + '_sift.npy'):
                        skip = False
                        
                if skip: 
                    continue
                    
                ofname = saveroot + d + '/image_' + str(i)
                #read the bounds
                txt = open(fname + '.txt','r')
                l = txt.readline()
                bounds = np.asarray([int(n) for n in l.split(' ')])
                txt.close()
                image = imread(fname + '.pgm',as_grey=True)
                sifts = [None] *  (maxScale + 1)

                interval = 5
                scaled = image
                for i in range(interval):
                    scale = pow(2.0, i / float(interval))
                    b = np.round(bounds * (1/ scale))
                    if scale > 1:
                        scaled = pyramid_reduce(image, scale)
                    sift  = extract_feature_dsift(scaled,spacing = 4,bounds = b)
                    #sift = pca.transform(sift)
                    np.save(ofname + '_l' + str(i) + '_sift.npy', np.asfarray(sift,dtype='float32'))
                    
                    if i + interval <= maxScale:
                        sift = extract_feature_dsift(scaled,spacing = 8, bounds = b)
                        #sift = pca.transform(sift)
                        np.save(ofname + '_l' + str(i + interval) + '_sift.npy', np.asarray(sift,dtype='float32'))
                    j = 2
                    while i + j * interval <= maxScale:
                        b = np.ceil(b * 0.5)
                        scaled = pyramid_reduce(scaled, 0.5)
                        sift = (extract_feature_dsift(scaled,spacing = 8, bounds = b))
                        #sift = pca.transform(sift)
                        np.save(ofname + '_l' + str(i + j * interval) + '_sift.npy', np.asarray(sift,dtype='float32'))
                        j = j + 1
