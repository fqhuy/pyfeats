# -*- coding: utf-8 -*-
"""
Created on Fri Nov  8 08:56:58 2013

@author: phan
"""
from Feature import for_all_patches, normalize, FeatureExtractor
import numpy as np
import math
from scipy.fftpack import dct

def extract_feature_stripe(image,vector_length, spacing = 0 \
, padding='symmetric', normalizer = 'unit'):
    vl = vector_length
    if spacing != 0:
        spc = spacing
    else:
        spc = vl
        
    nx = (image.shape[0] - vector_length) / spc + 1
    ny = (image.shape[1] - vector_length) / spc + 1
    features = np.zeros((image.shape[0] * ny + image.shape[1] * nx,vl))
    '''
    vdiff = image.shape[0] - vl
    hdiff = image.shape[1] - vl
    if vdiff < 0:
        image = np.pad(image,((0,abs(vdiff)),(0,0)),padding)
    if hdiff < 0:
        image = np.pad(image,((0,0),(0,abs(hdiff))),padding)
    '''
    
    for i in range(0,ny):
        for row in range(image.shape[0]):
            
            if normalizer == "unit":
                norm = np.linalg.norm(image[row,i * spc: i * spc + vl])
            elif normalizer == "weber":
                norm = np.linalg.norm(image[row,i * spc: i * spc + vl])
                norm = (norm) / (math.log(1 + norm/0.03))
            elif normalizer == "none":
                norm = 1
            #features[image.shape[0] * i + row] = np.fft.fft(image[row,i * vl: i* vl + vl] / norm).real
            #features[row] = np.fft.fft(image[row,0:vl] / norm).real
            features[image.shape[0] * i + row] = dct(image[row,i * spc: i * spc + vl] / norm)
            
    for i in range(0,nx):        
        for col in range(image.shape[1]):
            if normalizer == "unit":
                norm = np.linalg.norm(image[i * spc: i * spc + vl, col])
            elif normalizer == "weber":
                norm = np.linalg.norm(image[i * spc: i * spc + vl, col])
                norm = (norm) / (math.log(1 + norm/0.03))
            elif normalizer == "none":
                norm = 1
            #features[col] = np.fft.fft(image[0:vl,col] / norm).real
            #features[image.shape[0] * ny + image.shape[1] * i + col] \
            #= np.fft.fft(image[i * vl: i * vl + vl, col] / norm   ).real             
            features[image.shape[0] * ny + image.shape[1] * i + col] \
            = dct(image[i * spc: i * spc + vl, col] / norm)
        
    return features

class Stripe:
    def __init__(self,vector_length,spacing=0,padding='symmetric', \
    normalizer='unit'):
        self.vector_length = vector_length
        self.spacing = spacing
        self.padding = padding
        self.normalizer = normalizer
        
    def extract(self,image):
        return extract_feature_stripe(image,self.vector_length,self.spacing, \
        self.padding,self.normalizer)
        
    def size(self):
        return self.vector_length
