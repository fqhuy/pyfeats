# -*- coding: utf-8 -*-

import numpy as np
from skimage.transform import pyramid_reduce
import math

def build_indices(data_shape, interval=5, spacing=8, padding=1, maxScale=25):
    interval = 5
    image  = np.array(data_shape) #np.zeros(data_shape)
    scaled = image.copy()
    levels = [None] * (maxScale + 1)
    scales = [0.0] * (maxScale + 1)
    def _calc_size(_data, _spacing):
        ey = round(_data[0] / float(_spacing))
        ex = round(_data[1] / float(_spacing))
        return (ey + padding * 2, ex + padding * 2)
        
    for i in range(interval):
        scale = pow(2.0, -i / float(interval))
        scale_ = pow(2.0, i / float(interval))
        if scale < 1:
            scaled = np.round(image * scale)
            
        levels[i] = _calc_size(scaled, spacing)
        scales[i] = scale_
        #if i + interval <= maxScale:
        #    levels[i + interval] = _calc_size(scaled, spacing)
        #    scales[i + interval] = scale_ * 2

        j = 1
        while i + j * interval <= maxScale:
            scaled = np.round(scaled * 0.5)
            levels[i + interval * j] = _calc_size(scaled, spacing)
            scales[i + interval * j] = scale_ * 2 * j
            j = j + 1
            
    return levels , scales
            
if __name__ == "__main__":
    print(build_indices((500,500)))