# -*- coding: utf-8 -*-
"""
Created on Sat Oct 19 22:55:31 2013

@author: phan
"""
from Feature import normalize, FeatureExtractor
from sklearn import random_projection
import math
import numpy as np
from scipy.spatial.distance import euclidean as euc
from matplotlib import pyplot as plt

def for_all_patches(image, func, patch_size, spacing=1, padding='none'):
    feats = []
    w,h,_ = image.shape
    for row in range(0, h - patch_size[0] + 1, spacing):
        feats.append([])
        for col in range(0, w - patch_size[1] + 1, spacing):
            patch = image[row:row + patch_size[0], col:col + patch_size[1], :]
            feats[-1].append( func(patch) )
    return np.array(feats)
    
def make_circles(cx, r_step, a_step):
    circles = []
    for r1 in range(r_step-1, cx + 1, r_step):
        circles.append([])
        for i in np.arange(0, np.pi * 2, a_step):
            y1 = int(np.floor(math.sin(i) * r1)) + cx
            x1 = int(np.floor(math.cos(i) * r1)) + cx               
            y2 = int(np.floor(math.sin(i) * (r1 - r_step))) + cx
            x2 = int(np.floor(math.cos(i) * (r1 - r_step))) + cx
            circles[-1].append((x1,y1,x2,y2))
    return circles
            
def extract_feature_srp(image, circles = None, patch_size=(17,17), r_step='auto', a_step='auto',
    spacing=1,sorting_method="radial",norm_method="weber",padding='none'):
    """
    Extract Sorted Random Projection features from the image.
    Li Liu; Fieguth, P.; Gangyao Kuang; Hongbin Zha, "Sorted Random Projections for robust texture classification," Computer Vision (ICCV), 2011 IEEE International Conference on , vol., no., pp.391,398, 6-13 Nov. 2011
doi: 10.1109/ICCV.2011.6126267
    http://ieeexplore.ieee.org/xpls/abs_all.jsp?arnumber=6126267&tag=1
    """
    #precompute circles
    cx = math.trunc(patch_size[0] / 2)
    if circles is None:
        circles = make_circles(cx, r_step, a_step)
    #circles_ = np.vstack(map(lambda x: np.array(x), circles))

    #return
    
    #extract SRP features from a single patch
    def _extract_feature_srp_radial(array):
        #cx = math.trunc(array.shape[0] / 2) #, math.trunc(array.shape[1] / 2)
        deltas = []
        for r1 in range(len(circles)):
            delta = []
            for i in range(len(circles[r1])):
                x1,y1,x2,y2 = circles[r1][i]
                delta.append(euc(array[y1 , x1] , array[y2, x2]))
                #delta.append(array[y1, x1][0])
            delta.sort()
            #delta = list(np.array(delta) / np.linalg.norm(delta))
            #d = np.array(delta)
            #d -= d.mean()
            #d /= d.std()
            #delta = list(d)
            deltas.append(delta)
        #deltas = np.asfarray(deltas)
        deltas = np.asfarray(reduce(lambda a,b: a+b,deltas))
        #transformer = random_projection.SparseRandomProjection()
        #deltas = transformer.fit_transform(deltas)        
        return deltas
        
    #def _extract_feature_srp_ordinary(array):
    #    sampling_indices = np.random.random_integers(0,len(array)-1,r_dim)
    #    return normalize(array.flatten()[sampling_indices],norm_method)

    if sorting_method == "radial":
        #re = _extract_feature_srp_radial(image, circles)
        re = np.asfarray(for_all_patches(image,_extract_feature_srp_radial,patch_size,spacing,padding))
    #elif sorting_method == "none":
    #    re = np.asfarray(for_all_patches(image,_extract_feature_srp_ordinary,patch_size,spacing,padding))
    else: 
        re = np.array([]) 
    return re
    
class SRP(FeatureExtractor):
    def __init__(self,patch_size,spacing,r_dim,sorting_method='none',normalizer='weber',padding='none'):
        super(SRP, self).__init__(patch_size,spacing,normalizer,padding)
        self.r_dim = r_dim
        self.sorting_method = sorting_method
        self.norm_method = normalizer
        self.padding = padding
        
    def extract(self,image):
        #extract_feature
        return extract_feature_srp(image, self.r_dim, self.patch_size, \
        self.spacing,self.sorting_method,self.norm_method,self.padding)
        
    def size(self):
        return self.r_dim


if __name__ == "__main__":
    import scipy
    from sklearn.mixture import GMM
    from skimage.draw import polygon
    import matplotlib as mpl
    
    gmm = GMM(n_components=1)
    gmm.means_ = np.array([[20.,20.]])
    gmm.weights_ = np.array([1.])
    gmm.covars_ = np.array([[10., 10.]])
    gmm.converged_ = True
    idx = np.asfarray([(i,j) for i in range(40) for j in range(40)])
    probs = gmm.score(idx)
    probs = probs.reshape((40, 40))
    
    rr,cc=polygon(np.array([20,30,10]), np.array([10,30,30]))
    probs[rr, cc] = 0.8
    
    feat = extract_feature_srp(probs[:,:,np.newaxis], patch_size=(40,40), r_step=2, a_step=np.radians(5))
    
    circles = make_circles(20, r_step=2, a_step=np.radians(5))
    circles = np.array(circles).reshape((-1,4))
    ax = plt.subplot(111)
    ax.imshow(probs, cmap = mpl.cm.gray,interpolation='none')
    ax.scatter(circles[:, 0], circles[:, 1], s=40., c=feat[0,0],cmap = mpl.cm.jet)
    plt.show()