# -*- coding: utf-8 -*-
"""
Created on Sun Jan  5 20:32:17 2014

@author: phan
"""

#from libcpp.vector cimport vector
cimport numpy as np
import numpy as np

cimport vlfeat# as vl
#from cython.operator cimport dereference as deref, preincrement as inc
#from libcpp.vector cimport vector
#from cpython.pycapsule cimport PyCapsule_New, PyCapsule_GetPointer
#from cpython cimport PyObject
from cython cimport view

DEF VL_ERR_EOF =  5
DEF VL_TYPE_FLOAT   = 1     #/**< @c float type */
DEF VL_TYPE_DOUBLE  = 2     #/**< @c double type */
DEF VL_TYPE_INT8    = 3     #/**< @c ::vl_int8 type */
DEF VL_TYPE_UINT8   = 4     #/**< @c ::vl_uint8 type */
DEF VL_TYPE_INT16   = 5     #/**< @c ::vl_int16 type */
DEF VL_TYPE_UINT16  = 6     #/**< @c ::vl_uint16 type */
DEF VL_TYPE_INT32   = 7     #/**< @c ::vl_int32 type */
DEF VL_TYPE_UINT32  = 8     #/**< @c ::vl_uint32 type */
DEF VL_TYPE_INT64   = 9     #/**< @c ::vl_int64 type */
DEF VL_TYPE_UINT64  = 10    #/**< @c ::vl_uint64 type */
VL_TRUE = 1
VL_FALSE = 0
'''
        vl_type dataType                  
        vl_size dimension                
        vl_size numClusters              
        vl_size numData                  
        vl_size maxNumIterations        
        vl_size numRepetitions         
        int     verbosity               
        void *  means                    
        void *  covariances               
        void *  priors                     
        void *  posteriors                
        double * sigmaLowBound           
        VlGMMInitialization initialization 
        VlKMeans * kmeansInit             
        double LL 
        vl_bool kmeansInitIsOwner
'''
'''
cdef class SiftKeypoint:
    cdef public int o
    cdef public int ix
    cdef public int iy
    cdef public int iz
    cdef public float x
    cdef public float y
    cdef public float z
    cdef public float sigma
    
    def __init__(o, ix, iy, iz, x, y, z, sigma):
        self.o = o
        self.ix = ix
        self.iy = iy
        self.iz = iz
        self.x = x
        self.y = y
        self.z = z
        self.sigma = sigma

'''
    
cdef class SIFT(object):
    
    cdef public int width, height, n_octaves, n_levels, o_min, err
    cdef vlfeat.VlSiftFilt* self_
    def __init__(self, width, height, n_octaves, n_levels, o_min):
        self.width = width
        self.height = height
        self.n_octaves = n_octaves
        self.n_levels = n_levels
        self.o_min = o_min
        self.self_ = vlfeat.vl_sift_new(width, height, n_octaves, n_levels, o_min)
    
    property peak_thresh:
        def __get__(self):
            return vlfeat.vl_sift_get_peak_thresh(self.self_)
            
        
        def __set__(self, t):
            vlfeat.vl_sift_set_peak_thresh(self.self_, t)
        
    property edge_thresh:
        def __get__(self):
            return vlfeat.vl_sift_get_edge_thresh(self.self_)
            
        
        def __set__(self, t):
            vlfeat.vl_sift_set_edge_thresh(self.self_, t)
        
    cpdef set_norm_thresh(self, double t):
        vlfeat.vl_sift_set_norm_thresh(self.self_, t)
        
    property window_size:
        def __get__(self):
            return vlfeat.vl_sift_get_window_size(self.self_)
            
        
        def __set__(self, t):
            vlfeat.vl_sift_set_window_size(self.self_, t)
        
    cpdef set_magnif(self, double t):
        vlfeat.vl_sift_set_magnif(self.self_, t)
        
    cpdef detect(self, np.ndarray[float, ndim=2, mode="c"] image):
        cdef list keypoints = []
        self.err = vlfeat.vl_sift_process_first_octave(self.self_, &image[0,0])
        
        vlfeat.vl_sift_detect(self.self_)
        cdef const vlfeat.VlSiftKeypoint* kpoints =  vlfeat.vl_sift_get_keypoints(self.self_)
        cdef int n_kpoints = vlfeat.vl_sift_get_nkeypoints(self.self_)
        cdef int i = 0
        for i in range(n_kpoints):
            #kp = SiftKeypoint(kpoints[i].o,kpoints[i].ix,kpoints[i].iy,0,kpoints[i].x,kpoints[i].y,kpoints[i].s,kpoints[i].sigma)
            keypoints.append(kpoints[i])
        
        return keypoints
        
    cpdef next_octave(self):

        if self.err != VL_ERR_EOF:
            self.err = vlfeat.vl_sift_process_next_octave(self.self_)
        else:
            return None
        
        cdef keypoints = []
        vlfeat.vl_sift_detect(self.self_)
        cdef const vlfeat.VlSiftKeypoint* kpoints =  vlfeat.vl_sift_get_keypoints(self.self_)
        cdef int n_kpoints = vlfeat.vl_sift_get_nkeypoints(self.self_)
        cdef int i = 0
        for i in range(n_kpoints):
            keypoints.append(kpoints[i])        
        
        return keypoints
        
    def __del__(self):
        if self.self_ is not NULL:
            vlfeat.vl_sift_delete(self.self_)
            self.self_ = NULL
    
cdef class DSIFT:
    cdef public int width, height, step, bin_size, descriptor_size, n_keypoints
    cdef public tuple _bounds
    cdef public list _keypoints
    cdef vlfeat.VlDsiftFilter* self_
    def __init__(self,height,width,step=8,bin_size=5,bounds=None):
        self.width = width
        self.height = height
        self.step = step
        self.bin_size = bin_size
        self._bounds = bounds
        self._keypoints = []
        
        #cdef void* self_ = <void*> vlfeat.vl_dsift_new_basic(width, height, step, bin_size)
        #self.self_ = PyCapsule_New(<void*> self_, "vlfeat.VlDsiftFilter*", NULL)
        if width == height == -1:
            self.self_ = NULL
        else:
            self.self_ = vlfeat.vl_dsift_new_basic(width, height, step, bin_size)
    
    @property
    def keypoints(self):
        return self._keypoints

    @property
    def bounds(self):
        return self._bounds
        
    @bounds.setter
    def bounds(self, value):
        #cdef vlfeat.VlDsiftFilter* self_ = \
        #<vlfeat.VlDsiftFilter*> PyCapsule_GetPointer(self.self_, "vlfeat.VlDsiftFilter*")
        if type(value) == tuple:
            vlfeat.vl_dsift_set_bounds(self.self_, value[0], value[1], value[2], value[3])
            self._bounds = value
        
    cpdef process(self, np.ndarray[float, ndim=2, mode="c"] image):
        cdef vlfeat.VlDsiftFilter* dsift = NULL
        if self.self_ != NULL:
            dsift = self.self_
            #print('self.self_ is not NULL')
        else:
            #print('self.self_ is NULL')
            dsift = vl_dsift_new_basic(image.shape[1], image.shape[0], self.step, self.bin_size)
            
        vlfeat.vl_dsift_process(dsift,&image[0,0])
        cdef int size = vlfeat.vl_dsift_get_descriptor_size(dsift)
        cdef int n_keypoints = vl_dsift_get_keypoint_num(dsift)
        cdef const vlfeat.VlDsiftKeypoint* kp = vl_dsift_get_keypoints(dsift)
        for i in range(n_keypoints):
            self._keypoints.append([kp[i].x,kp[i].y])
        self.descriptor_size = size
        self.n_keypoints = n_keypoints
        cdef const float* descriptors = vlfeat.vl_dsift_get_descriptors(dsift)
        re = np.asarray(<float[:size * n_keypoints]> descriptors).reshape(n_keypoints,size).copy()
        if self.self_ == NULL and dsift != NULL:
            vlfeat.vl_dsift_delete(dsift)
            
        return re

    def __del__(self):
        if self.self_ != NULL:
            vlfeat.vl_dsift_delete(self.self_)


cdef class HOG:
    VlHogVariantDalalTriggs = 0
    VlHogVariantUoctti = 1
    cdef vlfeat.VlHog* self_
    
    def __init__(self,variant,n_orientations,transposed):
        self.self_ = vlfeat.vl_hog_new(variant,n_orientations,transposed)
        
    cpdef process(self, np.ndarray[float, ndim=2, mode="c"] image, int cell_size):
        cdef vlfeat.VlHog* self_ = self.self_
        cdef int width = image.shape[1]
        cdef int height = image.shape[0]
        cdef int n_channels = 1
        vlfeat.vl_hog_put_image(self_,&image[0,0],width,height,n_channels,cell_size)
        cdef int hw = vlfeat.vl_hog_get_width(self_)
        cdef int hh = vlfeat.vl_hog_get_height(self_)
        cdef int hd = vlfeat.vl_hog_get_dimension(self_)
        #self.width = hw
        #self.height = hh
        #self.n_dimensions = hd
        cdef np.ndarray[float,ndim=1,mode='c'] features =  \
        np.ascontiguousarray(np.zeros((hw * hd * hh), dtype="f",order='C'))
        vlfeat.vl_hog_extract(self_,&features[0])
        return features.reshape(hh,hw,hd)
        
    cpdef visualize(self, np.ndarray[float, ndim=3, mode='c'] hog_array):
        cdef int glyphSize = vlfeat.vl_hog_get_glyph_size(self.self_)
        cdef int imageHeight = glyphSize * hog_array.shape[0]
        cdef int imageWidth = glyphSize * hog_array.shape[1]
        cdef np.ndarray[float, ndim=2, mode='c'] image = \
        np.zeros((imageWidth , imageHeight),dtype='f',order='C')
        #vlfeat.vl_malloc(sizeof(float)*imageWidth*imageHeight)
        vlfeat.vl_hog_render(self.self_, &image[0,0], &hog_array[0,0,0], hog_array.shape[1], hog_array.shape[0])
        return image
        
    def __del__(self):
        if self.self_ != NULL:
            vlfeat.vl_hog_delete(self.self_)
            self.self_ = NULL

cdef class GMM:
    VlGMMKMeans = 0 #/**< Initialize GMM from KMeans clustering. */
    VlGMMRand = 1   #/**< Initialize GMM parameters by selecting points at random. */
    VlGMMCustom = 2
    cdef public int data_type, n_dimensions, n_components, _max_iter
    cdef public bool init_method
    cdef vlfeat.VlGMM* self_
    cdef public np.ndarray covars_
    cdef public np.ndarray priors_
    cdef public np.ndarray weights_
    cdef public np.ndarray means_
    
    def __init__(self, n_dimensions, n_components, max_iter = 50, data_type=VL_TYPE_FLOAT,init_method=0):
        self.data_type = data_type
        self.n_dimensions = n_dimensions
        self.n_components = n_components
        self.init_method = init_method
        self._max_iter = max_iter
        #cdef vlfeat.VlKMeans * = vlfeat.vl_kmeans_new()
        #if use_kmeans is True:
            
        cdef vlfeat.VlGMM * gmm =  vlfeat.vl_gmm_new (data_type, n_dimensions, n_components)
        vlfeat.vl_gmm_set_initialization(gmm, init_method)
        vlfeat.vl_gmm_set_max_num_iterations(gmm, max_iter)
        self.self_ =  gmm #PyCapsule_New(<void*> gmm, "vlfeat.VlGMM*", NULL)

    @property    
    def max_iter(self):
        return self._max_iter
        
    @max_iter.setter
    def max_iter(self, value):
        vlfeat.vl_gmm_set_max_num_iterations(self.self_, value)
        self._max_iter = value
        
    cpdef cluster(self,np.ndarray[float,ndim=2,mode="c"] data):
        cdef vlfeat.VlGMM* gmm = self.self_
        vlfeat.vl_gmm_cluster(gmm, &data[0,0], len(data))
        
        #diagonal case
        cdef float* c_covars = <float*> vlfeat.vl_gmm_get_covariances(gmm)
        cdef np.ndarray[float,ndim=1,mode="c"] covars = \
        np.asarray(<float[:self.n_components*self.n_dimensions]> c_covars)
        self.covars_ = covars.reshape(self.n_components, self.n_dimensions).copy()
        
        cdef float* c_priors = <float*> vlfeat.vl_gmm_get_priors(gmm)
        cdef view.array a = <float[:self.n_components]> c_priors
        cdef np.ndarray[float,ndim=1,mode="c"] priors = np.asarray(a)
        self.priors_ = priors.copy()
        self.weights_ = priors.copy()
        
        cdef float* c_means = <float*> vlfeat.vl_gmm_get_means(gmm)
        cdef np.ndarray[float,ndim=1,mode="c"] means = \
        np.asarray(<float[:self.n_components*self.n_dimensions]> c_means)
        self.means_ = means.reshape(self.n_components, self.n_dimensions).copy()
        
    def __del__(self):
        if self.self_ != NULL:
            vlfeat.vl_gmm_delete(self.self_)        

class FisherEncoder:
    VL_FISHER_FLAG_SQUARE_ROOT     =     (0x1 << 0)
    VL_FISHER_FLAG_NORMALIZED      =     (0x1 << 1)
    VL_FISHER_FLAG_IMPROVED        =     (VL_FISHER_FLAG_NORMALIZED | VL_FISHER_FLAG_SQUARE_ROOT)
    VL_FISHER_FLAG_FAST            =     (0x1 << 2)

    def __init__(self):
        pass
        #self.pointers = []
  
    def encode(self,int n_dimensions, int n_clusters, \
    np.ndarray[float ,ndim=2,mode="c"] data, int flags, 
    np.ndarray[float ,ndim=2,mode="c"] covariances=None, 
    np.ndarray[float ,ndim=2,mode="c"] means=None, 
    np.ndarray[float ,ndim=1,mode="c"] priors=None):
        
        cdef np.ndarray[float ,ndim=1,mode="c"] enc = np.zeros(2 * n_dimensions * n_clusters, \
        dtype='f',order="C")
        #self.pointers.append(enc)
        
        cdef vlfeat.VlGMM* gmm = vlfeat.vl_gmm_new(VL_TYPE_FLOAT, n_dimensions, n_clusters)
        
        if means is None or covariances is None or priors is None:
            vlfeat.vl_gmm_cluster(gmm, &data[0,0], len(data))
            vlfeat.vl_fisher_encode(&enc[0], VL_TYPE_FLOAT, \
            vlfeat.vl_gmm_get_means(gmm), n_dimensions, n_clusters, \
            vlfeat.vl_gmm_get_covariances(gmm), \
            vlfeat.vl_gmm_get_priors(gmm), \
            &data[0,0], len(data), \
            flags)
            
        else:
            vlfeat.vl_fisher_encode(&enc[0], VL_TYPE_FLOAT, \
            &means[0,0], n_dimensions, n_clusters, \
            &covariances[0,0], \
            &priors[0], \
            &data[0,0], len(data), \
            flags)
        vlfeat.vl_gmm_delete(gmm)
        return enc
        
    def dencode(self, float spacing, tuple patch_size, int n_dimensions, int n_clusters, \
    np.ndarray[float ,ndim=3,mode="c"] image, int flags, 
    np.ndarray[float ,ndim=2,mode="c"] covariances=None, 
    np.ndarray[float ,ndim=2,mode="c"] means=None, 
    np.ndarray[float ,ndim=1,mode="c"] priors=None, int step = 2):
        
        cdef np.ndarray[float, ndim=3,mode='c'] B = np.zeros((round(image.shape[0] / spacing), \
        round(image.shape[1]/spacing), n_clusters * 2 * image.shape[2] ),dtype='f',order='C')
        
        #cdef np.ndarray[float ,ndim=1,mode="c"] enc = np.zeros(2 * n_dimensions * n_clusters, \
        #dtype='f',order="C")
        cdef np.ndarray[float, ndim=3, mode='c'] patch
        
        cdef int psx, psy, co, ro
        cdef float col, row
        cdef int y, x
        y = 0
        for row in np.arange(0, image.shape[0] - patch_size[0]/2, spacing):
            y += 1
            x = 0
            for col in np.arange(0, image.shape[1] - patch_size[1]/2, spacing): 
                psx = patch_size[1]
                psy = patch_size[0]
                ro = round(row)
                co = round(col)
                if ro + psy > image.shape[0]: 
                    psy = image.shape[0] - ro
                if co + psx > image.shape[1]: 
                    psx = image.shape[1] - co
                patch = np.ascontiguousarray(image[ro:ro + psy:step, co:co + psx:step])
                
                if not np.all(patch == 0):
                    vlfeat.vl_fisher_encode(&B[y,x,0], VL_TYPE_FLOAT, \
                    &means[0,0], n_dimensions, n_clusters, \
                    &covariances[0,0], \
                    &priors[0], \
                    &patch[0,0,0], patch.shape[0] * patch.shape[1], \
                    flags)
                    x += 1
        return B
            
    def __del__(self):
        pass

cdef class HomogeneousKernelMap:
    cdef vlfeat.VlHomogeneousKernelMap * self_
    cdef public order
    def __init__(self, gamma = 1.0, order = 1, period = -1.0, \
    kernel_type=vlfeat.VlHomogeneousKernelChi2, \
    window_type=vlfeat.VlHomogeneousKernelMapWindowRectangular):
        self.self_ = vlfeat.vl_homogeneouskernelmap_new( \
        kernel_type, gamma, order, period, window_type)
        self.order = 1
    
    def encode(self, np.ndarray[double ,ndim=1,mode="c"] X):
        cdef int stride = len(X)
        cdef np.ndarray[double, ndim=2, mode='c'] psi = np.zeros((stride , 2 * self.order + 1), \
        dtype='d', order='C')
        
        for i in range(stride):
            x = X[i]
            vlfeat.vl_homogeneouskernelmap_evaluate_d(self.self_, &psi[0, 0], stride, x)
            #vlfeat.vl_homogeneouskernelmap_delete(x)
        return psi
        
    def __del__(self):
        if self.self_ != NULL:
            vlfeat.vl_homogeneouskernelmap_delete(self.self_)         