# -*- coding: utf-8 -*-
"""
Created on Fri Mar 14 21:49:56 2014

@author: phan
"""

import rfs

def worker(root, saveroot, classes, maxLevels = 5, interval = 5, cluster=False):
    padding = 25 #Always padd 25px because mr, sigma = 10
    for d in classes:
        print('extracting mr8 from ' + d)
        if cluster:
            g = vl.GMM(max_iter = 150, n_dimensions = 8, n_components = 25)
            feats = []#np.zeros(( 60 * 451250, 8), dtype  = 'f')
        if os.path.isdir(root + d):
            for i in range(0,50): #60 Images
                fname = root + d + '/image_' + str(i)
                ofname = saveroot + d + '/image_' + str(i)
                skip = False #os.path.isfile(ofname + '.npz')
                if not skip:
                    image = np.asfarray(imread(fname + '.pgm',as_grey=True) / 255.,dtype='f')
                    
                    for i in range(interval):
                        scale = pow(2.0, i / float(interval))
                        if scale > 1:
                            scaled = np.asarray(pyramid_reduce(image, scale), dtype='float32')
                        else:
                            scaled = image
                            
                        scaled_ = np.pad(scaled, (( padding, padding), \
                        (padding, padding)), 'constant')
                        feat = rfs.extract_mr8(scaled_)
                        np.save(ofname + '_l' + str(i) + '.npy', \
                        np.asarray(feat,dtype='float32'))                        
                        j = 1
                        while i + j * interval <= maxLevels:
                            scaled = np.asarray(pyramid_reduce(scaled, 2), dtype='float32')
                            scaled_ = np.pad(scaled, (( padding, padding),(padding, padding)), 'constant')
                            feat = rfs.extract_mr8(scaled_)
                            np.save(ofname + '_l' + str(i + j* interval) + '.npy', \
                            np.asarray(feat,dtype='float32'))
                            j += 1
                else:
                    if cluster:
                        feat = np.load(ofname + '.npy')
                        feats.append(feat[::2])
                
        if cluster:
            feats = np.concatenate(feats)
            feats = np.ascontiguousarray(feats)
            print('clustering ' + d)
            g.cluster(feats)
            np.savez(saveroot + 'gmm_' + d + '.npz', g.weights_,g.means_,g.covars_)
    
if __name__ == "__main__":
    import numpy as np
    import os
    from sklearn.cluster import KMeans
    from skimage.io import imread
    import vlfeat as vl
    import multiprocessing
    from skimage.transform import pyramid_reduce

    #root = '/media/phan/BIGDATA/CLIMAZ/SCM_CLIM2b/'
    root = '/media/phan/BIGDATA/CLIMAZ/'
    saveroot = '/media/phan/BIGDATA/CLIMAZ/'
    #saveroot = root[:-1] + '_OUTPUT/'
    #classes = [['fleece', 'fur', 'corduroy'], ['denim','knit1', 'leather'], ['boucle', 'lace']]
    classes = [["LANDSCAPE"]]
    
    jobs = []
    for cc in classes:
        #worker(root,saveroot,cc, 5, 5 )
        p = multiprocessing.Process(target=worker,args=(root,saveroot,cc,5,5))
        jobs.append(p)
        p.start()        
    '''
    dic = []
    for d in classes:
        g = vl.GMM(max_iter = 150, n_dimensions = 8, n_components = 25)
        feats = []#np.zeros(( 60 * 451250, 8), dtype  = 'f')
        if os.path.isdir(root + d):
            for i in range(0,60): #60 Images
                fname = root + d + '/image_' + str(i)
                ofname = saveroot + d + '/image_' + str(i)
                skip = os.path.isfile(ofname + '.npz')
                if not skip:
                    image = np.asfarray(imread(fname + '.pgm',as_grey=True) / 255.,dtype='f')
                    feat = rfs.extract_mr8(image)
                else:
                    feat = np.load(ofname + '.npz')['arr_0']
                feats.append(feat[::2])
                np.savez(ofname + '.npz', feat)
        
        feats = np.concatenate(feats)
        feats = np.ascontiguousarray(feats)
        print('clustering ' + d)
        g.cluster(feats)
        arr = np.array([g.weights_,g.means_,g.covars_])
        dic.append(g.means_)
        np.savez(saveroot + 'gmm_' + d + '.npz', arr)
        
    np.savez(saveroot + 'dictionary.npz', np.concatenate(dic))
    '''