# -*- coding: utf-8 -*-
"""
Created on Sun Jan  5 20:37:46 2014

@author: phan
"""

'''
cython  --cplus vlfeat.pyx 
python setup.py build_ext --inplace

'''

from distutils.core import setup
from os.path import join
import numpy as np
import warnings

def configuration(parent_package='climaz', top_path=None):
    from numpy.distutils.misc_util import Configuration
    from numpy.distutils.system_info import get_info, BlasNotFoundError

    config = Configuration('pyfeats', parent_package, top_path)
    
    vlfeat_c = ['sift.c','dsift.c','hog.c','fisher.c','gmm.c','kmeans.c','kdtree.c','generic.c', \
    'host.c','imopv.c','random.c','mathop.c','mathop_sse2.c','mathop_avx.c','imopv_sse2.c', 
    'homkermap.c']
    vlfeat_sources = ['vlfeat.cpp'] + [join('src', 'vlfeat', x) for x in vlfeat_c]
    
    #vlfeat_h = ['dsift.h', 'hog.h', 'fisher.h'
    vlfeat_depends = []#[join('src', 'vlfeat', '*.h')]

    config.add_extension('vlfeat',
                         sources=vlfeat_sources,
                         libraries=[],
                         define_macros=[('VL_DISABLE_AVX', None)],
                         include_dirs=[join('src','vlfeat'), np.get_include()],
                         depends=vlfeat_depends)
    
    
    lss_sources = ['lss.cpp', join('src','lss','ssdesc.cc')]
    lss_depends = [] #[join('src', 'lss', '*.h')]
    config.add_extension('lss', sources = lss_sources, \
    include_dirs=[join('src','lss'), np.get_include()], \
    depends = lss_depends)
    
    rfs_sources = ['rfs.c',join('src','rfs','anigauss.c')]
    config.add_extension('rfs', sources = rfs_sources, include_dirs=[join('src','rfs'), \
                                                                    np.get_include()], depends=[])
    
    #pmk_dir = join('src', 'libpmk-2.5', 'libpmk2')                                   
    #config.add_extension('pmk', sources=['PMK.cpp'], \
    #                     extra_objects=[join(pmk_dir,'libpmk.o'), join(pmk_dir,'libpmk_util.o')],
    #                     include_dirs=[pmk_dir, np.get_include()])
                         
    config.add_subpackage('tests')
    return config

if __name__ == '__main__':
    from numpy.distutils.core import setup
    print configuration(top_path='').todict()
    setup(**configuration(top_path='').todict())