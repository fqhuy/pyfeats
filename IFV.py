# -*- coding: utf-8 -*-
# Improved Fisher Vectors.

from Feature import for_all_patches, normalize, FeatureExtractor
import numpy as np
import vlfeat as vl
from sklearn import mixture as mx
import multiprocessing
from PIL import Image
import DSIFT
from sklearn.decomposition import PCA
from matplotlib import mpl, pyplot

def visualize1(ifvs, ifv_image, patch_size):
    from scipy.spatial.distance import euclidean
    
    m = np.zeros((ifv_image.shape[0:2]),dtype='f')
    for ifv in ifvs:
        filt = ifv.flatten()
        for row in range(ifv_image.shape[0] - patch_size):
            for col in range(ifv_image.shape[1] - patch_size):
                for y in range(4):
                    for x in range(4):
                        m[row,col] += np.dot(ifv[y,  x], ifv_image[row + y , col  + x]) #euclidean(ifv[y,  x], ifv_image[row + y , col  + x])
                #m[row,col] = ifv.dot(ifv_image[row,col].transpose()).sum()
                #m[row,col] += filt.dot(ifv_image[row : row +  patch_size, col : col + patch_size].flatten())
    print(str(m.max()) + ' ' + str(np.unravel_index(m.argmax(),m.shape)))
    #fig = pyplot.figure(1)
    #cmap = mpl.colors.LinearSegmentedColormap.from_list('my_colormap', ['black','white'], 256)
    #img = pyplot.imshow(m, interpolation='nearest', cmap = cmap, origin='lower')
    #pyplot.colorbar(img, cmap = cmap)
    #fig.show()
    pyplot.imshow(m)
    pyplot.show()
    
def visualize(ifvs, ifv_image):
    
    m = np.zeros((ifv_image.shape[0:2]),dtype='f')
    for ifv in ifvs:
        for row in range(ifv_image.shape[0]):
            for col in range(ifv_image.shape[1]):
                #m[row,col] = ifv.dot(ifv_image[row,col].transpose()).sum()
                m[row,col] += ifv.dot(ifv_image[row,col].flatten())
    print(m.max())
    #fig = pyplot.figure(1)
    #cmap = mpl.colors.LinearSegmentedColormap.from_list('my_colormap', ['black','white'], 256)
    #img = pyplot.imshow(m, interpolation='nearest', cmap = cmap, origin='lower')
    #pyplot.colorbar(img, cmap = cmap)
    #fig.show()
    pyplot.imshow(m)
    pyplot.show()

def extract_feature_ifv_once(data, gmm,n_clusters=0):
    fisher = vl.FisherEncoder() 
    if gmm == None:
        #gmm = mx.GMM(n_components=n_components)
        #gmm.fit(data)
        ifv = fisher.encode(data.shape[1] , n_clusters, np.asarray(data,dtype='f',order='C'), \
        vl.FisherEncoder.VL_FISHER_FLAG_IMPROVED)
        
    if isinstance(gmm, mx.GMM):
        ifv = fisher.encode(data.shape[1], gmm.n_components, \
        np.asarray(data,dtype='f',order='C'), \
        vl.FisherEncoder.VL_FISHER_FLAG_IMPROVED, \
        np.asarray(gmm.covars_,dtype='f',order='C'), 
        np.asarray(gmm.means_,dtype='f',order='C'), 
        np.asarray(gmm.weights_,dtype='f',order='C'))
        
    elif isinstance(gmm, dict):
        ifv = fisher.encode(data.shape[1], gmm['ncoms'], \
        np.asarray(data,dtype='f',order='C'), \
        vl.FisherEncoder.VL_FISHER_FLAG_IMPROVED, \
        np.asarray(gmm['covars'],dtype='f',order='C'), 
        np.asarray(gmm['means'],dtype='f',order='C'), 
        np.asarray(gmm['weights'],dtype='f',order='C'))        
    return ifv
    
def extract_feature_ifv(image,gmm = None, n_components=20, patch_size = (16,16), \
    spacing = 1, sampling_indices = None, normalizer = 'weber', padding='none'):
    def _extract_feature_ifv(patch, gmm):
        data = patch
        fisher = vl.FisherEncoder()
        
        if gmm == None:
            gmm = mx.GMM(n_components=n_components)
            gmm.fit(data)
        
        ifv = fisher.encode(data.shape[1], gmm.n_components, \
        np.asarray(data,dtype='f',order='C'), \
        vl.FisherEncoder.VL_FISHER_FLAG_IMPROVED, \
        np.asarray(gmm.covars_,dtype='f',order='C'), 
        np.asarray(gmm.means_,dtype='f',order='C'), 
        np.asarray(gmm.weights_,dtype='f',order='C'))
        
        #ifv = fisher.encode(data.shape[1],n_components, np.ascontiguousarray(data,dtype='f'), \
        #vl.FisherEncoder.VL_FISHER_FLAG_IMPROVED)
        return ifv
    
    B = np.zeros((round(image.shape[0] / spacing), round(image.shape[1]/spacing), n_components * 2, image.shape[2] ),dtype='f')
    for row in np.arange(0, image.shape[0] - patch_size[0]/2, spacing):
        #print('row: ' + str(row))
        for col in np.arange(0, image.shape[1] - patch_size[1]/2, spacing):
            #print('col: ' + str(col) )
            psx = patch_size[1]
            psy = patch_size[0]
            ro = round(row)
            co = round(col) 
            if ro + psy > image.shape[0]: 
                psy = image.shape[0] - ro
            if co + psx > image.shape[1]: 
                psx = image.shape[1] - co
            patch = image[ro:ro + psy:2, co:co + psx:2]
            if not np.all(patch == 0):
                c = _extract_feature_ifv(patch.reshape((patch.shape[0] * patch.shape[1], patch.shape[2])), gmm)
                B[round(row / spacing), round(col / spacing)] = c.reshape(n_components * 2, image.shape[2])
            
    return B
    
class IFV(FeatureExtractor):
    def __init__(self,gmm,n_components,patch_size=(16,16),spacing=1,normalizer='weber',padding='none'):
        super(IFV, self).__init__(patch_size,spacing,normalizer,padding)
        self.n_components = n_components
        self.gmm = gmm
        
    def extract(self,image,dtype='SIFT'):
        #img = image #img_as_float(data.lena()[:256, :256].mean(axis=2))
        #return extract_feature_ifv(image,self.gmm, self.n_components,self.patch_size, self.spacing)
        fisher = vl.FisherEncoder()
        gmm = self.gmm
        ifv = fisher.dencode(self.spacing, self.patch_size, image.shape[2], gmm.n_components, \
        np.asarray(image,dtype='f',order='C'), \
        vl.FisherEncoder.VL_FISHER_FLAG_IMPROVED, \
        np.asarray(gmm.covars_,dtype='f',order='C'), 
        np.asarray(gmm.means_,dtype='f',order='C'), 
        np.asarray(gmm.weights_,dtype='f',order='C'))
        
        return ifv
        
    def size(self):
        return 0

def worker(classes,saveroot,lgg,levels,gmm):
    padding = 1
    for d in classes:
        lgg.info('---------------- CLASS %s ------------------' % d)
        if os.path.isdir(saveroot + d):
            for i in range(0,60):
                lgg.info('processing image: %s' % str(i))
                #fname = root + d + '/image_' + str(i)
                ofname = saveroot + d + '/image_' + str(i)
                #ann = open(fname + '.txt', 'r')
                #l = ann.readline()
                #r = [int(n) for n in l.split(' ')]
                ifv = IFV(spacing=2,gmm=gmm,n_components=200)
                scale = 1
                for level in range(1, 6):
                   #if os.path.isfile(ofname + '_l' + str(level) + '_ifv_65.npy'):
                   #    continue
                   if not os.path.isfile(ofname + '_l' + str(level) + '.npy'):
                       continue
                   
                   data = np.load(ofname + '_l' + str(level) + '.npy')
                   #data = np.load(ofname + '.npy')
                   data = data.reshape(np.sqrt(data.shape[0]),np.sqrt(data.shape[0]), data.shape[1])
                   scale = data.shape[0] / 1000.
                   
                   size = levels[level + interval * 2]
                   sz = size[0] - padding * 2
                   
                   spacing = data.shape[0] / float(sz)
                   ifv.spacing = spacing
                   ifv.patch_size = (128 * scale, 128 * scale)
                   
                   feature = ifv.extract(data)
                   feature = np.pad(feature, ((padding, padding),(padding, padding),(0,0)), 'constant')
                   np.save(ofname + '_l' + str(level) + '_ifv_65.npy', np.asfarray(feature, dtype='float32'))
                #Load SIFT Pyramid
                '''
                pyramid = np.load(fname + '.npy')
                ifv = IFV(2,8)
                for i in range(1:21):
                    l = ann.readline()[0:-1]
                    r = [int(n) for n in l.split(' ')]
                    #hog = vl.HOG(vl.HOG.VlHogVariantUoctti,8,vl.VL_FALSE)
                    ifvs = []
                    for layer in pyramid:
                        ifvs.append(ifv.extract(layer[r[0]:r[1], r[2]:r[3]]))
                        #h = hog.process(np.ascontiguousarray(layer[:,:,np.newaxis],dtype='f'), \
                        #layer.shape[0], layer.shape[1], 1, 8)
                        #ifvs.append(h)
                    #Save Improved Fisher Vectors
                    np.save(fname + '_' + str(i) + '_ifv.npy', np.asarray(ifvs))
                '''
                #ann.close()

def worker1(classes, root, saveroot, lgg, gmm):
    pca = PCA(n_components=64)
    pca_data = np.load(saveroot + 'pca.npy')
    pca.components_ = pca_data[0]
    pca.explained_variance_ratio_ = pca_data[1]
    pca.mean_ = pca_data[2]
    
    for d in classes:
        lgg.info('---------------- CLASS %s ------------------' % d)
        if os.path.isdir(saveroot + d):
            for i in range(60):
                lgg.info('processing image: %s' % str(i))
                fname = root + d + '/image_' + str(i)
                ofname = saveroot + d + '/image_' + str(i)

                #ifv = IFV(spacing=2,gmm=gmm,n_components=200)
                ff = []
                for p in range(1,21):
                    im = np.asarray(Image.open(fname + '_' + str(p) + '.pgm').convert('F'))
                    f = DSIFT.extract_feature_dsift(im,spacing=4,padding=0)
                    f = f.reshape((f.shape[0] * f.shape[1], f.shape[2]))
                    f = pca.transform(f)
                    f1 = extract_feature_ifv_once(f,gmm)
                    np.save(ofname + '_part%d_ifv.npy' % p, f1)
                    ff.append(f)
                data = np.vstack(ff)
                fff = extract_feature_ifv_once(data,gmm)
                np.save(ofname + '_allparts_ifv.npy', fff)

def worker3(classes, root, saveroot, lgg, gmm):
    for d in classes:
        lgg.info('---------------- CLASS %s ------------------' % d)
        if os.path.isdir(saveroot + d):
            for i in range(60):
                lgg.info('processing image: %s' % str(i))
                fname = root + d + '/image_' + str(i)
                ofname = saveroot + d + '/image_' + str(i)

                ff = []
                txt = open(fname +'.txt','r')
                txt.readline() #Throw away the 0th part                
                
                im = np.load(ofname + '.npy').reshape((1000,1000,8))

                for p in range(1,21):
                    l = txt.readline()
                    rect = np.asarray([int(n) for n in l.split(' ')])
                    x = (rect[0]) #/ 32 #16
                    y = (rect[1]) #/ 32 #16
                    #sz = (rect[2] - rect[0]) / 32
                    sz = 128
                    patch = im[y: y + sz : 2, x : x + sz : 2].reshape((64 * 64, 8))
                    ff.append(patch)
                txt.close()
                data = np.vstack(ff)
                fff = extract_feature_ifv_once(data,gmm)
                np.save(ofname + '_allparts_ifv.npy', fff)

#extract from parts of images which belong to the same pose
def worker4(classes, root, saveroot, lgg, gmm, n_clusters):
    for d in classes:
        lgg.info('---------------- CLASS %s ------------------' % d)
        labels = np.load(saveroot + d + '_pose_clusters_labels.npy')
        if os.path.isdir(saveroot + d):
            fff = []
            for cluster in range(n_clusters):
                image_ids = [idx for (idx, l) in zip(range(60), labels) if l == cluster]
                ff = []
                for i in image_ids:
                    lgg.info('processing image: %s' % str(i))
                    fname = root + d + '/image_' + str(i)
                    ofname = saveroot + d + '/image_' + str(i)
    
                    
                    txt = open(fname +'.txt','r')
                    txt.readline() #Throw away the 0th part                
                    
                    im = np.load(ofname + '.npy').reshape((1000,1000,8))
    
                    for p in range(1,21):
                        l = txt.readline()
                        rect = np.asarray([int(n) for n in l.split(' ')])
                        x = (rect[0]) #/ 32 #16
                        y = (rect[1]) #/ 32 #16
                        #sz = (rect[2] - rect[0]) / 32
                        sz = 128
                        patch = im[y: y + sz : 2, x : x + sz : 2].reshape((64 * 64, 8))
                        ff.append(patch)
                    txt.close()
                    
                data = np.vstack(ff)
                fff.append( extract_feature_ifv_once(data,gmm) )
            
            np.save(saveroot + d + '_ifv_clusters.npy', np.asarray(fff,dtype='f'))
                    
def worker2(files, saveroot, ran, gmm):
    for f in files[ran[0]:ran[1]]:
        ifv = IFV(spacing=1,gmm=gmm,n_components=200)
        for level in range(1):
            data = np.load(saveroot + f[:-4] + '_l' + str(level)+'_sift.npy')
            feature = np.asarray(ifv.extract(data), dtype='float')
            np.save(saveroot +  f.replace('_sift','_ifv'), feature)
           
if __name__ == "__main_":

    import os
    import pyramid as pyr
    import logging
    from skimage.io import imread
    root = '/media/phan/BIGDATA/CLIMAZ/NEG/'
    saveroot = root[:-1] + '_OUTPUT/' 
    #levels = pyr.build_indices((1000,1000))   

    
    gmm = mx.GMM(n_components=200)
    gmm.means_ = np.load(saveroot + 'hgmm_means.npy')
    assert(gmm.means_.shape == (200, 64))
    gmm.covars_ = np.load(saveroot + 'hgmm_covars.npy')
    assert(gmm.covars_.shape == (200, 64))
    gmm.weights_ = np.load(saveroot + 'hgmm_weights.npy')
    assert(gmm.weights_.shape[0] == 200)    
    
    files = os.listdir(root)
    files = [f for f in files if f.endswith('.jpg')]
    ifv = IFV(spacing=1, patch_size=(16,16),gmm=gmm,n_components=200)
    #splits = [(0,3),(3,6),(6,10)]
    splits = [(9,10)]
    for split in splits:
        p = multiprocessing.Process(target=worker2,args=(files,saveroot,split,gmm))
        #obs.append(p)
        p.start()        
#This script reads SIFT Pyramids & writes IFV data
if __name__ == "__main__":
    #from skimage import data    
    #from skimage.transform import pyramid_gaussian, pyramid_expand
    #from skimage import img_as_ubyte
    import os
    import pyramid as pyr
    import logging
    
    #root
    levels, scales = pyr.build_indices((1000,1000))
 
    root = '/media/phan/BIGDATA/CLIMAZ/SCM_CLIM2b/'
    saveroot = root[:-1] + '_OUTPUT/'
    interval = 5
    #for d in os.listdir(root):
    #classes = [['denim', 'corduroy'],[ 'fleece', 'fur'],['knit1']]
    #classes = [['leather'], ['boucle'], ['lace']]
    classes = [['fleece', 'fur'], ['corduroy', 'denim'],['knit1', 'leather'], ['boucle', 'lace', 'LANDSCAPE']]
    #classes = [['LANDSCAPE']]
    flat_classes = ['fleece', 'fur', 'corduroy', 'denim','knit1', 'leather', 'boucle', 'lace']
    #root = '/media/phan/BIGDATA/CLIMAZ/SCM_CLIM2b'
    #saveroot = '/media/phan/BIGDATA/CLIMAZ/SCM_CLIM2b_OUTPUT/'
    #classes = [["LANDSCAPE_OUTPUT"]]    
    
    gmm = mx.GMM(n_components=200)
    gmm.means_  = []
    gmm.covars_ = []
    gmm.weights_ = []
    for c in flat_classes:
        gmmdata = np.load(saveroot + 'gmm_' + c + '.npz')
        gmm.means_.append(gmmdata['arr_1'])
        gmm.covars_.append(gmmdata['arr_2'])
        gmm.weights_.append(gmmdata['arr_0'])
        
    gmm.means_ = np.concatenate(gmm.means_)
    gmm.covars_ = np.concatenate(gmm.covars_)
    gmm.weights_ = np.concatenate(gmm.weights_)
    np.savez('gmm_all.npz',gmm.weights_, gmm.means_, gmm.covars_)
    
    assert(gmm.means_.shape == (200, 8))
    assert(gmm.covars_.shape == (200, 8))
    assert(gmm.weights_.shape[0] == 200)
    #saveroot = '/media/phan/BIGDATA/CLIMAZ/'
    '''
    gmm.means_ = np.load(saveroot + 'hgmm_means.npy')
    assert(gmm.means_.shape == (200, 64))
    gmm.covars_ = np.load(saveroot + 'hgmm_covars.npy')
    assert(gmm.covars_.shape == (200, 64))
    gmm.weights_ = np.load(saveroot + 'hgmm_weights.npy')
    assert(gmm.weights_.shape[0] == 200)
    '''
    
    jobs = []
    for cx,cs in enumerate(classes):
        #lgg.basicConfig(filename=root + "features_%d_ifv.log" % cx, level=lgg.DEBUG, \
        #format='%(asctime)s:%(levelname)s:%(message)s')
        # create logger with 'spam_application'
        logger = logging.getLogger("features_%d_ifv" % cx)
        logger.setLevel(logging.DEBUG)
        # create file handler which logs even debug messages
        fh = logging.FileHandler(saveroot + "features_%d_ifv.log" % cx)
        fh.setLevel(logging.DEBUG)
        # create formatter and add it to the handlers
        formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
        fh.setFormatter(formatter)
        # add the handlers to the logger
        logger.addHandler(fh)
        #p = multiprocessing.Process(target=worker3,args=(cs,root, saveroot,logger,gmm))
        p = multiprocessing.Process(target=worker,args=(cs,saveroot,logger,levels,gmm))
        #p = multiprocessing.Process(target=worker1,args=(cs,root,saveroot ,logger,gmm))
        #p = multiprocessing.Process(target=worker4,args=(cs,root,saveroot,logger,gmm,4))
        #worker(cs,saveroot,logger,levels,gmm)
        jobs.append(p)
        p.start()
