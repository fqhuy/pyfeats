# -*- coding: utf-8 -*-
"""
Created on Sun Oct 20 09:19:30 2013

@author: phan
"""

from Feature import for_all_paches, normalize, FeatureExtractor
from Utils import *

def extract_feature_curvature(image,scales=[2,4,16]):
    """
    
    parameters:
    - input: should be an edge detected image.
    returns:
    - something
    """
    def _extract_feature_curvature(patch):
        contours = measure.find_contours(im2, 0.8)
        curvatures = []
        for n, contour in enumerate(contours):
            #plt.plot(contour[:, 1], contour[:, 0], linewidth=2)

            for m, point in enumerate(contour):
                for scale in scales:
                    curvature_vector = np.array([0.,0.,0.])
                    vec1 = point - contour[m - scale]
                    vec2 = point - contour[m + scale]
                    a = np.linalg.norm(vec1)
                    v = vecangle(vec1,vec2)/2
                    b = np.sin(v) * a
                    r = b / np.sin(180 - v * 2)
                    curvature_vector[scale] = 1. / r
            curvatures.append(curvature_vector)
        return np.asfarray(curvatures)
    
    return for_all_patches(image,_extract_feature_curvature)
            
class Curvature(FeatureExtractor):
    def __init__(self,scales):
        super(SRP, self).__init__(patch_size,spacing,None)
        self.scales = scales
        
    def extract(image):
        return extract_feature_curvature(image, self.scales)
        
    def size():
        return 0