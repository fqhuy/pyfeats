# -*- coding: utf-8 -*-

import vlfeat as vl
from skimage.io import imread
import numpy as np
import cv2

def extract_feature_sift(image, n_kpoints, n_layers=3, mask=None ,lib='cv2', \
contrast_thresh=0.04, edge_thresh=10, sigma=1.6):
    if lib is 'cv2':
        
        if cv2.__version__ == '3.0.0':
            #print 'cv2 version', cv2.__version__
            sift = cv2.xfeatures2d.SIFT_create(n_kpoints, n_layers, \
            contrastThreshold=contrast_thresh, edgeThreshold=edge_thresh,sigma=sigma)
        else:
            sift = cv2.SIFT(n_kpoints, n_layers)
            
        _kps, feats = sift.detectAndCompute(image, mask)
        kps = np.zeros((len(_kps), 5),dtype='f4')
        for k in range(len(_kps)):
            kps[k][0] = _kps[k].pt[0]
            kps[k][1] = _kps[k].pt[1]
            kps[k][2] = _kps[k].angle
            kps[k][3] = _kps[k].size
            kps[k][4] = _kps[k].octave
            
        points = np.array([p.pt for p in _kps])
    elif lib is 'vlfeat':
        sift = vl.SIFT(image.shape[1] ,  image.shape[0], 2, 3, 0)
        sift.peak_thresh = 0.01
        sift.edge_thresh = 20
        kps = sift.detect(np.ascontiguousarray(image, dtype='float32'))
        points = np.array( [[p['x'], p['y']] for p in kps] )
        feats = None
        
    return points, kps, feats

if __name__ == '__main__':
    url = "/media/phan/BIGDATA/CLIMAZ/SCM_CLIM2b/corduroy/image_53.pgm"
    image = imread(url, as_grey=True) / 1.
    extract_feature_sift(image)