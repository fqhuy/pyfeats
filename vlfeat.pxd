from libcpp cimport bool

DEF VL_DISABLE_AVX = 1

IF UNAME_MACHINE == "x86_64":
    ctypedef long long           vl_int64 #;   /**< @brief Signed 64-bit integer. */
    ctypedef int                 vl_int32 #;   /**< @brief Signed 32-bit integer. */
    ctypedef short               vl_int16 #;   /**< @brief Signed 16-bit integer. */
    ctypedef char                vl_int8  #;   /**< @brief Signed  8-bit integer. */
    
    ctypedef long long          vl_uint64 #;  /**< @brief Unsigned 64-bit integer. */
    ctypedef int                vl_uint32 #;  /**< @brief Unsigned 32-bit integer. */
    ctypedef short              vl_uint16 #;  /**< @brief Unsigned 16-bit integer. */
    ctypedef char               vl_uint8 #;   /**< @brief Unsigned  8-bit integer. */
    
    ctypedef int                 vl_int #;     /**< @brief Same as @c int. */
    ctypedef unsigned int        vl_uint #;    /**< @brief Same as <code>unsigned int</code>. */
    
    ctypedef int                 vl_bool #;    /**< @brief Boolean. */
    ctypedef vl_int64            vl_intptr #;  /**< @brief Integer holding a pointer. */
    ctypedef vl_uint64           vl_uintptr #; /**< @brief Unsigned integer holding a pointer. */
    ctypedef vl_uint64           vl_size #;    /**< @brief Unsigned integer holding the size of a memory block. */
    ctypedef vl_int64            vl_index #;   /**< @brief Signed version of ::vl_size and ::vl_uindex */
    ctypedef vl_uint64           vl_uindex #;  /**< @brief Same as ::vl_size */

ctypedef vl_uint32 vl_type
ctypedef float vl_sift_pix

DEF VL_FISHER_FLAG_SQUARE_ROOT     =     (0x1 << 0)
DEF VL_FISHER_FLAG_NORMALIZED      =     (0x1 << 1)
DEF VL_FISHER_FLAG_IMPROVED        =     (VL_FISHER_FLAG_NORMALIZED | VL_FISHER_FLAG_SQUARE_ROOT)
DEF VL_FISHER_FLAG_FAST            =     (0x1 << 2)

cdef extern from "mathop.h":
    ctypedef float (*VlFloatVectorComparisonFunction)(vl_size dimension, float * X, float  * Y)
    ctypedef double (*VlDoubleVectorComparisonFunction)(vl_size dimension, double  * X, double  * Y)
    ctypedef float (*VlFloatVector3ComparisonFunction)(vl_size dimension, float  * X, float  * Y, float  * Z)
    ctypedef double (*VlDoubleVector3ComparisonFunction)(vl_size dimension, double  * X, double  * Y, double  * Z)

    ctypedef enum VlVectorComparisonType:
      VlDistanceL1,        #/**< l1 distance (squared intersection metric) */
      VlDistanceL2,        #/**< squared l2 distance */
      VlDistanceChi2,      #/**< squared Chi2 distance */
      VlDistanceHellinger, #/**< squared Hellinger's distance */
      VlDistanceJS,        #**< squared Jensen-Shannon distance */
      VlDistanceMahalanobis,     #/**< squared mahalanobis distance */
      VlKernelL1,          #/**< intersection kernel */
      VlKernelL2,          #/**< l2 kernel */
      VlKernelChi2,        #/**< Chi2 kernel */
      VlKernelHellinger,   #/**< Hellinger's kernel */
      VlKernelJS           #/**< Jensen-Shannon kernel */
      
cdef extern from "sift.h":
    ctypedef struct VlSiftKeypoint:
        int o
        int ix
        int iy
        int izzz #work around
        float x
        float y
        float s
        float sigma

    
    ctypedef struct VlSiftFilt:
        pass
    
    VlSiftFilt*  vl_sift_new    (int width, int height,
                                 int noctaves, int nlevels,
                                 int o_min)
    
    void         vl_sift_delete (VlSiftFilt *f)
    
    int   vl_sift_process_first_octave       (VlSiftFilt *f,
                                              vl_sift_pix *im)
    int   vl_sift_process_next_octave        (VlSiftFilt *f)

    void  vl_sift_detect                     (VlSiftFilt *f)

    int   vl_sift_calc_keypoint_orientations (VlSiftFilt *f,
                                              double angles [4],
                                              VlSiftKeypoint *k)
                                              
    void  vl_sift_calc_keypoint_descriptor   (VlSiftFilt *f,
                                              vl_sift_pix *descr,
                                              VlSiftKeypoint * k,
                                              double angle)

    void  vl_sift_calc_raw_descriptor        (VlSiftFilt *f,
                                              vl_sift_pix * image,
                                              vl_sift_pix *descr,
                                              int widht, int height,
                                              double x, double y,
                                              double s, double angle0)

    void  vl_sift_keypoint_init              (VlSiftFilt *f,
                                              VlSiftKeypoint *k,
                                              double x,
                                              double y,
                                              double sigma)

    inline int    vl_sift_get_octave_index   (VlSiftFilt *f)
    inline int    vl_sift_get_noctaves       (VlSiftFilt *f)
    inline int    vl_sift_get_octave_first   (VlSiftFilt *f)
    inline int    vl_sift_get_octave_width   (VlSiftFilt *f)
    inline int    vl_sift_get_octave_height  (VlSiftFilt *f)
    inline int    vl_sift_get_nlevels        (VlSiftFilt *f)
    inline int    vl_sift_get_nkeypoints     (VlSiftFilt *f)
    inline double vl_sift_get_peak_thresh    (VlSiftFilt *f)
    inline double vl_sift_get_edge_thresh    (VlSiftFilt *f)
    inline double vl_sift_get_norm_thresh    (VlSiftFilt *f)
    inline double vl_sift_get_magnif         (VlSiftFilt *f)
    inline double vl_sift_get_window_size    (VlSiftFilt *f)
    
    inline vl_sift_pix *vl_sift_get_octave  (VlSiftFilt *f, int s)
    inline VlSiftKeypoint *vl_sift_get_keypoints (VlSiftFilt *f)

    inline void vl_sift_set_peak_thresh (VlSiftFilt *f, double t)
    inline void vl_sift_set_edge_thresh (VlSiftFilt *f, double t)
    inline void vl_sift_set_norm_thresh (VlSiftFilt *f, double t)
    inline void vl_sift_set_magnif      (VlSiftFilt *f, double m)
    inline void vl_sift_set_window_size (VlSiftFilt *f, double m)    

cdef extern from "dsift.h":
    ctypedef struct VlDsiftKeypoint:
        double x
        double y
        double s
        double norm

    ctypedef struct VlDsiftDescriptorGeometry:
        int numBinT
        int numBinX
        int numBinY
        int binSizeX
        int binSizeY

    ctypedef struct VlDsiftFilter:
        int imWidth #;            /**< @internal @brief image width */
        int imHeight #;           /**< @internal @brief image height */
            
        int stepX #;              /**< frame sampling step X */
        int stepY #;              /**< frame sampling step Y */
            
        int boundMinX #;          /**< frame bounding box min X */
        int boundMinY #;          /**< frame bounding box min Y */
        int boundMaxX #;          /**< frame bounding box max X */
        int boundMaxY #;          /**< frame bounding box max Y */
            
        #/** descriptor parameters */
        VlDsiftDescriptorGeometry geom
            
        int useFlatWindow #;      /**< flag: whether to approximate the Gaussian window with a flat one */
        double windowSize #;      /**< size of the Gaussian window */
            
        int numFrames #;          /**< number of sampled frames */
        int descrSize #;          /**< size of a descriptor */
        VlDsiftKeypoint *frames #; /**< frame buffer */
        float *descrs #;          /**< descriptor buffer */
            
        int numBinAlloc #;        /**< buffer allocated: descriptor size */
        int numFrameAlloc #;      /**< buffer allocated: number of frames  */
        int numGradAlloc #;       /**< buffer allocated: number of orientations */
            
        float **grads #;          /**< gradient buffer */
        float *convTmp1 #;        /**< temporary buffer */
        float *convTmp2 #;        /**< temporary buffer */

    VlDsiftFilter *vl_dsift_new (int width, int height)
    VlDsiftFilter *vl_dsift_new_basic (int width, int height, int step, int binSize)
    
    void vl_dsift_delete (VlDsiftFilter *self_)
    void vl_dsift_process (VlDsiftFilter *self_, float* im)
    void vl_dsift_transpose_descriptor (float* dst, float * src, int numBinT, \
    int numBinX, int numBinY)
    
    void vl_dsift_set_steps (VlDsiftFilter *self_,  int stepX, int stepY)
    void vl_dsift_set_bounds (VlDsiftFilter *self_, int minX, int minY,  int maxX, int maxY)
    void vl_dsift_set_geometry (VlDsiftFilter *self_, VlDsiftDescriptorGeometry * geom)
    void vl_dsift_set_flat_window (VlDsiftFilter *self_, vl_bool useFlatWindow)
    void vl_dsift_set_window_size (VlDsiftFilter *self_, double windowSize)
    
    float* vl_dsift_get_descriptors(VlDsiftFilter *self_)
    int vl_dsift_get_descriptor_size(VlDsiftFilter *self_)
    int vl_dsift_get_keypoint_num(VlDsiftFilter *self_)
    VlDsiftKeypoint *vl_dsift_get_keypoints (VlDsiftFilter *self_)
    void            vl_dsift_get_bounds          (VlDsiftFilter *self_, int* minX, int* minY, int* maxX, int* maxY)
    void            vl_dsift_get_steps           (VlDsiftFilter *self_, int* stepX, int* stepY)
    VlDsiftDescriptorGeometry *vl_dsift_get_geometry (VlDsiftFilter *self_)
    vl_bool         vl_dsift_get_flat_window     (VlDsiftFilter *self_)
    double          vl_dsift_get_window_size     (VlDsiftFilter  *self_) 

cdef extern from "kmeans.h":
    ctypedef enum VlKMeansAlgorithm:
      VlKMeansLloyd,       
      VlKMeansElkan,       
      VlKMeansANN          
    
    ctypedef enum VlKMeansInitialization:
      VlKMeansRandomSelection,  
      VlKMeansPlusPlus          

    ctypedef struct VlKMeans:
        vl_type dataType #                      /**< Data type. */
        vl_size dimension #                     /**< Data dimensionality. */
        vl_size numCenters #                    /**< Number of centers. */
        vl_size numTrees #                      /**< Number of trees in forest when using ANN-kmeans. */
        vl_size maxNumComparisons #             /**< Maximum number of comparisons when using ANN-kmeans. */
        VlKMeansInitialization initialization # /**< Initalization algorithm. */
        VlKMeansAlgorithm algorithm #           /**< Clustring algorithm. */
        VlVectorComparisonType distance #       /**< Distance. */
        vl_size maxNumIterations #              /**< Maximum number of refinement iterations. */
        vl_size numRepetitions #                /**< Number of clustering repetitions. */
        int verbosity #                         /**< Verbosity level. */
        
        void * centers #                        /**< Centers */
        void * centerDistances #                /**< Centers inter-distances. */
        
        double energy #                         /**< Current solution energy. */
        VlFloatVectorComparisonFunction floatVectorComparisonFn #
        VlDoubleVectorComparisonFunction doubleVectorComparisonFn #
        
        VlKMeans * vl_kmeans_new (vl_type dataType, VlVectorComparisonType distance)
        VlKMeans * vl_kmeans_new_copy (VlKMeans  * kmeans)
        void vl_kmeans_delete (VlKMeans * self_)
        void vl_kmeans_reset (VlKMeans * self_)
        double vl_kmeans_cluster (VlKMeans * self_, void  * data, \
                                    vl_size dimension, vl_size numData, \
                                    vl_size numCenters)

        void vl_kmeans_quantize (VlKMeans * self_,  vl_uint32 * assignments, \
                                   void * distances,  void  * data, \
                                   vl_size numData)

        void vl_kmeans_quantize_ANN (VlKMeans * self_, vl_uint32 * assignments, \
                                   void * distances, void  * data, \
                                   vl_size numData,  vl_size iteration )

        void vl_kmeans_set_centers (VlKMeans * self_, void  * centers, \
                                      vl_size dimension, vl_size numCenters)

        void vl_kmeans_init_centers_with_rand_data(VlKMeans * self_, void  * data, \
                   vl_size dimensions, vl_size numData, vl_size numCenters)

        void vl_kmeans_init_centers_plus_plus(VlKMeans * self_, void  * data, \
                   vl_size dimensions, vl_size numData, vl_size numCenters)

        double vl_kmeans_refine_centers (VlKMeans * self_, void  * data, vl_size numData)


        inline vl_type vl_kmeans_get_data_type (VlKMeans  * self_)
        inline VlVectorComparisonType vl_kmeans_get_distance (VlKMeans  * self_)

        inline VlKMeansAlgorithm vl_kmeans_get_algorithm (VlKMeans  * self_)
        inline VlKMeansInitialization vl_kmeans_get_initialization (VlKMeans  * self_)
        inline vl_size vl_kmeans_get_num_repetitions (VlKMeans  * self_)

        inline vl_size vl_kmeans_get_dimension (VlKMeans  * self_)
        inline vl_size vl_kmeans_get_num_centers (VlKMeans  * self_)

        inline int vl_kmeans_get_verbosity (VlKMeans  * self_)
        inline vl_size vl_kmeans_get_max_num_iterations (VlKMeans  * self_)
        inline vl_size vl_kmeans_get_max_num_comparisons (VlKMeans  * self_)
        inline vl_size vl_kmeans_get_num_trees (VlKMeans  * self_)
        inline double vl_kmeans_get_energy (VlKMeans  * self_)
        inline void * vl_kmeans_get_centers (VlKMeans  * self_)

        inline void vl_kmeans_set_algorithm (VlKMeans * self_, VlKMeansAlgorithm algorithm)
        inline void vl_kmeans_set_initialization (VlKMeans * self_, VlKMeansInitialization initialization)
        inline void vl_kmeans_set_num_repetitions (VlKMeans * self_, vl_size numRepetitions)
        inline void vl_kmeans_set_max_num_iterations (VlKMeans * self_, vl_size maxNumIterations)
        inline void vl_kmeans_set_verbosity (VlKMeans * self_, int verbosity_)
        inline void vl_kmeans_set_max_num_comparisons (VlKMeans * self_, vl_size maxNumComparisons)
        inline void vl_kmeans_set_num_trees (VlKMeans * self_, vl_size numTrees)
      

cdef extern from "gmm.h":
    ctypedef enum VlGMMInitialization:
        VlGMMKMeans, #/**< Initialize GMM from KMeans clustering. */
        VlGMMRand,   #/**< Initialize GMM parameters by selecting points at random. */
        VlGMMCustom  #/**< User specifies the initial GMM parameters. */
        
    ctypedef struct VlGMM:
        pass
    
    VlGMM* vl_gmm_new (double dataType, vl_size dimension, vl_size numComponents)
    VlGMM * vl_gmm_new_copy (VlGMM * gmm)
    void vl_gmm_delete (VlGMM * self_)
    void vl_gmm_reset (VlGMM * self_)
    double vl_gmm_cluster(VlGMM * self_, void * data, vl_size numData)
    
    void vl_gmm_init_with_rand_data \
    (VlGMM * self_, \
     void * data, \
     vl_size numData)

    void vl_gmm_init_with_kmeans \
    (VlGMM * self_, \
     void * data, \
     vl_size numData, \
     VlKMeans * kmeansInit)
    
    double vl_gmm_em(VlGMM * self_, void * data, vl_size numData)
    
    void vl_gmm_set_means(VlGMM * self_, void * means)
    
    void vl_gmm_set_covariances(VlGMM * self_,  void * covariances)
    
    void vl_gmm_set_priors(VlGMM * self_, void * priors)
    
    double vl_get_gmm_data_posteriors_f(float * posteriors, \
                                 vl_size numClusters, \
                                 vl_size numData, \
                                 float * priors, \
                                 float * means, \
                                 vl_size dimension, \
                                 float * covariances, \
                                 float * data)
    
    double vl_get_gmm_data_posteriors_d(double * posteriors, \
                                 vl_size numClusters, \
                                 vl_size numData, \
                                 double * priors, \
                                 double * means, \
                                 vl_size dimension, \
                                 double * covariances, \
                                 double * data)
        
    void * vl_gmm_get_means (VlGMM  * self_)
    void * vl_gmm_get_covariances (VlGMM  * self_)
    void * vl_gmm_get_priors (VlGMM  * self_)
    void * vl_gmm_get_posteriors (VlGMM  * self_)
    vl_type vl_gmm_get_data_type (VlGMM * self_)
    vl_size vl_gmm_get_dimension (VlGMM * self_)
    vl_size vl_gmm_get_num_repetitions (VlGMM * self_)
    vl_size vl_gmm_get_num_data (VlGMM * self_)
    vl_size vl_gmm_get_num_clusters (VlGMM * self_)
    double vl_gmm_get_loglikelyhood (VlGMM * self_)
    int vl_gmm_get_verbosity (VlGMM  * self_)
    vl_size vl_gmm_get_max_num_iterations (VlGMM * self_)
    vl_size vl_gmm_get_num_repetitions (VlGMM * self_)
    VlGMMInitialization vl_gmm_get_initialization (VlGMM * self_)
    VlKMeans * vl_gmm_get_kmeans_init_object (VlGMM * self_)
    double * vl_gmm_get_covariance_lower_bounds (VlGMM * self_) 
    
    void vl_gmm_set_num_repetitions (VlGMM * self_, vl_size numRepetitions)
    void vl_gmm_set_max_num_iterations (VlGMM * self_, vl_size maxNumIterations)
    void vl_gmm_set_verbosity (VlGMM * self_, int verbosity)
    void vl_gmm_set_initialization (VlGMM * self_, VlGMMInitialization init)
    void vl_gmm_set_kmeans_init_object (VlGMM * self_, VlKMeans * kmeans)
    void vl_gmm_set_covariance_lower_bounds (VlGMM * self_, double * bounds)
    void vl_gmm_set_covariance_lower_bound (VlGMM * self_, double bound)
    
cdef extern from "fisher.h":
    void vl_fisher_encode(void * enc, vl_type dataType, \
    void  * means, vl_size dimension, vl_size numClusters, \
    void  * covariances, \
    void  * priors, \
    void  * data, vl_size numData, \
    int flags)
    
cdef extern from "hog.h":
    ctypedef enum VlHogVariant:
        VlHogVariantDalalTriggs, VlHogVariantUoctti
        
    ctypedef struct VlHog:
        VlHogVariant variant
        vl_size dimension
        vl_size numOrientations
        vl_bool transposed
        vl_bool useBilinearOrientationAssigment
        #/* left-right flip permutation */
        vl_index * permutation
        #/* glyphs */
        float * glyphs
        vl_size glyphSize
        
        #/* helper vectors */
        float * orientationX
        float * orientationY
        #/* buffers */
        float * hog
        float * hogNorm
        vl_size hogWidth
        vl_size hogHeight
        
    VlHog * vl_hog_new (VlHogVariant variant, vl_size numOrientations, bool transposed)
    
    void vl_hog_delete (VlHog * self_)
    
    void vl_hog_process (VlHog * self_, \
    float * features, \
    float * image, \
    vl_size width, vl_size height, vl_size numChannels, \
    vl_size cellSize)

    void vl_hog_put_image (VlHog * self_, \
    float * image, \
    vl_size width, vl_size height, \
    vl_size numChannels,vl_size cellSize)
    
    void vl_hog_put_polar_field (VlHog * self_, \
    float * modulus, \
    float * angle, \
    vl_bool directed, \
    vl_size width, vl_size height, \
    vl_size cellSize)
    
    void vl_hog_extract (VlHog * self_, float * features)
    vl_size vl_hog_get_height (VlHog * self_)
    vl_size vl_hog_get_width (VlHog * self_)
    vl_size vl_hog_get_dimension (VlHog * self_)
    void vl_hog_render (VlHog * self, \
                                  float * image, \
                                  float * features, \
                                  vl_size width, \
                                  vl_size height)
    
    vl_index * vl_hog_get_permutation (VlHog * self)
    vl_size vl_hog_get_glyph_size (VlHog * self)
    
    vl_bool vl_hog_get_use_bilinear_orientation_assignments (VlHog * self)
    void vl_hog_set_use_bilinear_orientation_assignments (VlHog * self, vl_bool x)
    
cdef extern from "homkermap.h":
    ctypedef struct VlHomogeneousKernelMap
    #typedef struct _VlHomogeneousKernelMap VlHomogeneousKernelMap
    
    ctypedef enum VlHomogeneousKernelType:
        VlHomogeneousKernelIntersection, \
        VlHomogeneousKernelChi2, \
        VlHomogeneousKernelJS

    ctypedef enum VlHomogeneousKernelMapWindowType:
        VlHomogeneousKernelMapWindowUniform, \
        VlHomogeneousKernelMapWindowRectangular

    VlHomogeneousKernelMap * vl_homogeneouskernelmap_new (VlHomogeneousKernelType kernelType, \
                                 double gamma, \
                                 vl_size order, \
                                 double period, \
                                 VlHomogeneousKernelMapWindowType windowType)
                                 
    void vl_homogeneouskernelmap_delete (VlHomogeneousKernelMap * self_)
    
    void vl_homogeneouskernelmap_evaluate_d (VlHomogeneousKernelMap * self_, \
                                        double * destination, \
                                        vl_size stride, \
                                        double x)
    
    void vl_homogeneouskernelmap_evaluate_f (VlHomogeneousKernelMap * self_, \
                                        float * destination, \
                                        vl_size stride, \
                                        double x)
    
    vl_size vl_homogeneouskernelmap_get_order (VlHomogeneousKernelMap * self_)
    
    vl_size vl_homogeneouskernelmap_get_dimension (VlHomogeneousKernelMap * self_)
    
    VlHomogeneousKernelType vl_homogeneouskernelmap_get_kernel_type (VlHomogeneousKernelMap * self_)
    
    VlHomogeneousKernelMapWindowType vl_homogeneouskernelmap_get_window_type (VlHomogeneousKernelMap * self_)